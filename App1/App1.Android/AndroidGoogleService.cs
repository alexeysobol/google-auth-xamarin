﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Auth;
using Xamarin.Forms;
using App1.Droid;
using App1.Interface;
using App1.Model;
using System.Threading.Tasks;
using System.Diagnostics;
using Newtonsoft.Json;

[assembly: Dependency(typeof(AndroidGoogleService))]
namespace App1.Droid
{
    class AndroidGoogleService : IGoogleService
    {
        LoginResult _loginResult;
        TaskCompletionSource<LoginResult> _completionSource;

        Account account;
        AccountStore store;
        public AndroidGoogleService()
        {
            store = AccountStore.Create();
            account = store.FindAccountsForService(Constants.AppName).FirstOrDefault();
        }
        public Task<LoginResult> Login()
        {
            _completionSource = new TaskCompletionSource<LoginResult>();

            string clientId = null;
            string redirectUri = null;

            switch (Device.RuntimePlatform)
            {
                //case Device.iOS:
                //    clientId = Constants.iOSClientId;
                //    redirectUri = Constants.iOSRedirectUrl;
                //    break;

                case Device.Android:
                    clientId = Constants.AndroidClientId;
                    redirectUri = Constants.AndroidRedirectUrl;
                    break;
            }

            var authenticator = new OAuth2Authenticator(
                clientId: clientId,
                clientSecret: null,
                scope: Constants.Scope,
                authorizeUrl: new Uri(Constants.AuthorizeUrl),
                redirectUrl: new Uri(redirectUri),
                accessTokenUrl: new Uri(Constants.AccessTokenUrl),
                getUsernameAsync: null,
                isUsingNativeUI: true)
            {
                AllowCancel = true
            };

            authenticator.Completed += OnAuthCompleted;
            authenticator.Error += OnAuthError;
            authenticator.ClearCookiesBeforeLogin = true;

            AuthenticationState.Authenticator = authenticator;


            var presenter = new Xamarin.Auth.Presenters.OAuthLoginPresenter();
            presenter.Login(authenticator);


            return _completionSource.Task;
        }


        async void OnAuthCompleted(object sender, AuthenticatorCompletedEventArgs e)
        {
            var authenticator = sender as OAuth2Authenticator;
            if (authenticator != null)
            {
                authenticator.Completed -= OnAuthCompleted;
                authenticator.Error -= OnAuthError;
            }
            
            if (e.IsAuthenticated)
            {
                
                var request = new OAuth2Request("GET", new Uri(Constants.UserInfoUrl), null, e.Account);
                var response = await request.GetResponseAsync();
                if (response != null)
                {
                    string userJson = await response.GetResponseTextAsync();
                    var result = JsonConvert.DeserializeObject<dynamic>(userJson);

                    _loginResult = new LoginResult()
                    {
                        FirstName = result.given_name,
                        LastName = result.family_name,
                        ImageUrl = result.picture,
                        Token = e.Account.Properties["access_token"],
                        UserId = result.id,
                        LoginState = LoginState.Success
                    };
                }
                _completionSource?.TrySetResult(_loginResult);

                if (account != null)
                {
                    store.Delete(account, Constants.AppName);
                }

                await store.SaveAsync(account = e.Account, Constants.AppName);
            }
        }

        void OnAuthError(object sender, AuthenticatorErrorEventArgs e)
        {
            var authenticator = sender as OAuth2Authenticator;
            if (authenticator != null)
            {
                authenticator.Completed -= OnAuthCompleted;
                authenticator.Error -= OnAuthError;
            }

            _completionSource?.TrySetResult(new LoginResult
            {
                LoginState = LoginState.Failed,
                ErrorString = e.Message
            });
            Debug.WriteLine("Authentication error: " + e.Message);
        }

        public async void Logout()
        {
            var request = new OAuth2Request("GET", new Uri("https://accounts.google.com/logout"), null, account);
            await request.GetResponseAsync();
        }
    }
}