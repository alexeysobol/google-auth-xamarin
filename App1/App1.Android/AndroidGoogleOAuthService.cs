﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using App1.Interface;
using App1.Model;
using Xamarin.Auth;
using Xamarin.Forms;
using System.Net;
using Newtonsoft.Json.Linq;
using App1.Droid;
using Android.Provider;

[assembly: Dependency(typeof(AndroidGoogleOAuthService))]
namespace App1.Droid
{
    class AndroidGoogleOAuthService : IGoogleService
    {
        private Account account { get; set; }
        TaskCompletionSource<LoginResult> _completionSource;
        public static readonly string GOOGLE_ID =
                             "6673259735-eqqamo0svlnld9suth2clmj4ptptfrcg.apps.googleusercontent.com";
        public static readonly string GOOGLE_SCOPE =
                                 "https://www.googleapis.com/auth/userinfo.email";
        public static readonly string GOOGLE_AUTH =
                                   "https://accounts.google.com/o/oauth2/auth";
        public static readonly string GOOGLE_REDIRECTURL =
                                  "https://www.googleapis.com/plus/v1/people/me";

        private async void AuthOnCompleted(object sender, AuthenticatorCompletedEventArgs authCompletedArgs)
        {
            if (!authCompletedArgs.IsAuthenticated || authCompletedArgs.Account == null)
                SetResult(new LoginResult { LoginState = LoginState.Canceled });
            else
            {
                var token = authCompletedArgs.Account.Properties.ContainsKey("access_token")
                    ? authCompletedArgs.Account.Properties["access_token"]
                    : null;
                var expInString = authCompletedArgs.Account.Properties.ContainsKey("expires_in")
                    ? authCompletedArgs.Account.Properties["expires_in"]
                    : null;

                var expireIn = Convert.ToInt32(expInString);
                var expireAt = DateTimeOffset.Now.AddSeconds(expireIn);

                account = authCompletedArgs.Account;
                await GetUserProfile(authCompletedArgs.Account, token, expireAt);
            }
        }

        public Task<LoginResult> Login()
        {
            _completionSource = new TaskCompletionSource<LoginResult>();
            var auth = new OAuth2Authenticator
                       (
                         clientId: GOOGLE_ID,
                         scope: GOOGLE_SCOPE,
                         authorizeUrl: new Uri(GOOGLE_AUTH),
                         redirectUrl: new Uri(GOOGLE_REDIRECTURL)
                        );

            auth.OnPageLoading(new Uri("https://accounts.google.com/login" + "?client_id=" + GOOGLE_ID + "&redirect_uri=" + GOOGLE_REDIRECTURL));
            
            ////Simple code for ope web browser in android
            //Intent webPageIntent = new Intent(Intent.ActionView);
            //webPageIntent.SetData(Android.Net.Uri.Parse("https://accounts.google.com/login" + "?client_id=" + GOOGLE_ID + "&redirect_uri=" + GOOGLE_REDIRECTURL));
            //Forms.Context.StartActivity(webPageIntent);

            //Android.Webkit.WebView webView = new Android.Webkit.WebView(Forms.Context);

            //webView.LoadUrl("https://accounts.google.com/login" + "?client_id=" + GOOGLE_ID + "&redirect_uri=" + GOOGLE_REDIRECTURL);

            //auth.AccessTokenUrl = new Uri("https://accounts.google.com/o/oauth2/token");
            //var presenter = new Xamarin.Auth.Presenters.OAuthLoginPresenter();
            //presenter.Login(auth);


            auth.AllowCancel = true;
            auth.ClearCookiesBeforeLogin = true;
            auth.Title = "Google +";

            auth.Completed += AuthOnCompleted;
            //Used to launch the corresponding social Login page
            //Forms.Context.StartActivity(auth.GetUI(Forms.Context));

            return _completionSource.Task;
        }

        public void Logout()
        {
            throw new NotImplementedException();
        }
        async Task GetUserProfile(Account account, string token, DateTimeOffset expireAt)
        {
            var request = new OAuth2Request
                             (
                                "GET",
                                 new Uri("https://www.googleapis.com/oauth2/v2/userinfo"),
                                 null,
                                 account
                             );

            //Get response here
            var response = await request.GetResponseAsync();

            var result = new LoginResult();
            if (response != null && response.StatusCode == HttpStatusCode.OK)
            {

                var userJson = response.GetResponseText();


                var jobject = JObject.Parse(userJson);
                var name = jobject["name"]?.ToString().Split(new char[] { ' ' });

                result.Token = token;
                result.ExpireAt = expireAt;
                result.LoginState = LoginState.Success;
                result.FirstName = name[0];
                result.LastName = name[1];
                result.ImageUrl = jobject["picture"]?["data"]?["url"]?.ToString();
            }
            else
            {
                result.LoginState = LoginState.Failed;
                result.ErrorString = $"Error: Responce={response}, StatusCode = {response?.StatusCode}";
            }

            SetResult(result);
        }

        void SetResult(LoginResult result)
        {
            _completionSource?.TrySetResult(result);
            _completionSource = null;
        }
    }
}