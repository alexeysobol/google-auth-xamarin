﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using App1.Interface;
using Xamarin.Facebook;
using Xamarin.Forms;
using App1.Droid;
using App1.Model;
using System.Threading.Tasks;
using Org.Json;

[assembly: Dependency(typeof(AndroidFacebookService))]
namespace App1.Droid
{
    public class AndroidFacebookService : Java.Lang.Object, IFacebookServiceAut, GraphRequest.IGraphJSONObjectCallback, GraphRequest.ICallback, IFacebookCallback
    {
        public static AndroidFacebookService Instance => DependencyService.Get<IFacebookServiceAut>() as AndroidFacebookService;

        readonly ICallbackManager _callbackManager = CallbackManagerFactory.Create();
        readonly string[] _permissions = { @"public_profile", @"email", @"user_about_me" };

        LoginResult _loginResult;
        TaskCompletionSource<LoginResult> _completionSource;

        public AndroidFacebookService()
        {
            Xamarin.Facebook.Login.LoginManager.Instance.RegisterCallback(_callbackManager, this);
        }

        public Task<LoginResult> Login()
        {
            _completionSource = new TaskCompletionSource<LoginResult>();
            Xamarin.Facebook.Login.LoginManager.Instance.LogInWithReadPermissions(Forms.Context as Activity, _permissions);
            return _completionSource.Task;
        }

        public void Logout()
        {
            Xamarin.Facebook.Login.LoginManager.Instance.LogOut();
        }

        public void OnActivityResult(int requestCode, int resultCode, Intent data)
        {
            _callbackManager?.OnActivityResult(requestCode, resultCode, data);
        }

        public void OnCompleted(JSONObject data, GraphResponse response)
        {
            OnCompleted(response);
        }
        public void OnCompleted(GraphResponse response)
        {
            if (response?.JSONObject == null)
                _completionSource?.TrySetResult(new LoginResult { LoginState = LoginState.Canceled });
            else
            {
                _loginResult = new LoginResult
                {
                    FirstName = Profile.CurrentProfile?.FirstName,
                    LastName = Profile.CurrentProfile?.LastName,
                    ImageUrl = response.JSONObject?.GetJSONObject("picture")?.GetJSONObject("data")?.GetString("url"),
                    Token = AccessToken.CurrentAccessToken?.Token,
                    UserId = AccessToken.CurrentAccessToken?.UserId,
                    LoginState = LoginState.Success
                };

                _completionSource?.TrySetResult(_loginResult);
            }
        }

        public void OnCancel()
        {
            _completionSource?.TrySetResult(new LoginResult { LoginState = LoginState.Canceled });
        }

        public void OnError(FacebookException exception)
        {
            _completionSource?.TrySetResult(new LoginResult
            {
                LoginState = LoginState.Failed,
                ErrorString = exception?.Message
            });
        }

        public void OnSuccess(Java.Lang.Object result)
        {
            var facebookLoginResult = result.JavaCast<Xamarin.Facebook.Login.LoginResult>();
            if (facebookLoginResult == null) return;

            var parameters = new Bundle();
            parameters.PutString("fields", "id,email,picture.width(1000).height(1000)");
            var request = GraphRequest.NewMeRequest(facebookLoginResult.AccessToken, this);
            request.Parameters = parameters;
            request.ExecuteAsync();
        }
    }
}