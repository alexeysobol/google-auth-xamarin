﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//using Android.App;
//using Android.Content;
//using Android.OS;
//using Android.Runtime;
//using Android.Views;
//using Android.Widget;
//using System.Threading.Tasks;
//using App1.Interface;
//using App1.Model;
//using Xamarin.Auth;
//using Xamarin.Forms;
//using System.Net;
//using Newtonsoft.Json;
//using App1.Droid;
//using Newtonsoft.Json.Linq;

//[assembly: Dependency(typeof(AndroidAuthFacebookService))]
//namespace App1.Droid
//{
//    class AndroidAuthFacebookService : Java.Lang.Object, IFacebookService
//    {
//        TaskCompletionSource<LoginResult> _completionSource;
//        private Account account { get; set; }

//        public Task<LoginResult> Login()
//        {
//            _completionSource = new TaskCompletionSource<LoginResult>();
//            var auth = new OAuth2Authenticator
//            (
//                clientId: "287588715088758",
//                scope: "",
//                authorizeUrl: new Uri("https://m.facebook.com/dialog/oauth/"),
//                redirectUrl: new Uri("https://www.facebook.com/connect/login_success.html")
//            )
//            {
//                AllowCancel = true
//            };

//            auth.Completed += AuthOnCompleted;
//            auth.ClearCookiesBeforeLogin = true;
//            auth.Title = "Microsoft";

//            Forms.Context.StartActivity(auth.GetUI(Forms.Context));

//            return _completionSource.Task;
//        }

//        private async void AuthOnCompleted(object sender, AuthenticatorCompletedEventArgs authCompletedArgs)
//        {
//            if (!authCompletedArgs.IsAuthenticated || authCompletedArgs.Account == null)
//                SetResult(new LoginResult { LoginState = LoginState.Canceled });
//            else
//            {
//                var token = authCompletedArgs.Account.Properties.ContainsKey("access_token")
//                    ? authCompletedArgs.Account.Properties["access_token"]
//                    : null;
//                var expInString = authCompletedArgs.Account.Properties.ContainsKey("expires_in")
//                    ? authCompletedArgs.Account.Properties["expires_in"]
//                    : null;

//                var expireIn = Convert.ToInt32(expInString);
//                var expireAt = DateTimeOffset.Now.AddSeconds(expireIn);

//                account = authCompletedArgs.Account;
//                await GetUserProfile(authCompletedArgs.Account, token, expireAt);
//            }
//        }

//        public void Logout()
//        {
//            _completionSource = null;
//        }

//        void SetResult(LoginResult result)
//        {
//            _completionSource?.TrySetResult(result);
//            _completionSource = null;
//        }

//        async Task GetUserProfile(Account account, string token, DateTimeOffset expireAt)
//        {

//            var request = new OAuth2Request("GET", new Uri("https://graph.facebook.com/me?fields=name,picture,email"),
//               null, account);


//            var response = await request.GetResponseAsync();
//            var result = new LoginResult();
//            if (response != null && response.StatusCode == HttpStatusCode.OK)
//            {

//                var userJson = response.GetResponseText();


//                var jobject = JObject.Parse(userJson);
//                var name = jobject["name"]?.ToString().Split(new char[] { ' ' });

//                result.Token = token;
//                result.ExpireAt = expireAt;
//                result.LoginState = LoginState.Success;
//                result.FirstName = name[0];
//                result.LastName = name[1];
//                result.ImageUrl = jobject["picture"]?["data"]?["url"]?.ToString();
//            }
//            else
//            {
//                result.LoginState = LoginState.Failed;
//                result.ErrorString = $"Error: Responce={response}, StatusCode = {response?.StatusCode}";
//            }

//            SetResult(result);
//        }
//    }
//}