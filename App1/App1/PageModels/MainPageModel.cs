﻿using App1.Interface;
using App1.Model;
using FreshMvvm;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Auth;
using Xamarin.Forms;

namespace App1.PageModels
{
    public class MainPageModel: FreshBasePageModel
    {
        public Command<string> AuthorizationCommand => new Command<string>(async (TypeSocial) =>
        {

            LoginResult loginResult = null;

            switch (TypeSocial)
            {
                case "Facebook":
                    loginResult = await DependencyService.Get<IFacebookServiceAut>().Login();
                    loginResult.TypeSocial = Model.TypeSocial.Facebook;
                    break;
                case "Vk":
                    loginResult = await DependencyService.Get<IVkServiceAut>().Login();
                    loginResult.TypeSocial = Model.TypeSocial.Vk;
                    break;
                case "Google":
                    loginResult = await DependencyService.Get<IGoogleService>().Login();
                    loginResult.TypeSocial = Model.TypeSocial.Google;
                    break;
            }

            switch (loginResult.LoginState)
            {
                case LoginState.Canceled:
                    break;
                case LoginState.Success:

                    var container = new FreshMasterDetailNavigationContainer();
                    container.Master = FreshPageModelResolver.ResolvePageModel<MenuItemPageModel>(loginResult);
                    container.Detail = new FreshNavigationContainer(FreshPageModelResolver.ResolvePageModel<MainPageModel>());

                    await App.Current.MainPage.Navigation.PushModalAsync(container, true);


                    break;
                default:
                    break;
            }
        });
    }
}
