﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FreshMvvm;
using App1.Model;
using Xamarin.Forms;
using App1.Interface;
using App1.Facebook;
using App1.Vk;
using App1.Google;

namespace App1.PageModels
{
    class InfoUserPageModel: FreshBasePageModel
    {

        private bool _isBusy { get; set; }
        public bool IsBusy
        {
            get
            {
                return _isBusy;
            }
            set
            {
                _isBusy = value;
                RaisePropertyChanged();
            }
        }
        private InfoUserModel _infoUser { get; set; }
        public InfoUserModel InfoUser
        {
            get
            {
                return _infoUser;
            }
            set
            {
                if (_infoUser != value)
                {
                    _infoUser = value;
                    RaisePropertyChanged();
                }
            }
        }

        public InfoUserPageModel()
        {
            IsBusy = true;
        }

        public override async void Init(object initData)
        {
            Tuple<string, TypeSocial> tuple = initData as Tuple<string, TypeSocial>;

            switch (tuple.Item2)
            {
                case TypeSocial.Vk:
                    InfoUser = await DependencyService.Get<VkService>().GetAccountAsync(tuple.Item1);
                    break;
                case TypeSocial.Facebook:
                    InfoUser = await DependencyService.Get<FacebookService>().GetAccountAsync(tuple.Item1);
                    break;
                case TypeSocial.Google:
                    InfoUser = await DependencyService.Get<GoogleServices>().GetAccountAsync(tuple.Item1);
                    break;
            }

            IsBusy = false;
        }

    }
}
