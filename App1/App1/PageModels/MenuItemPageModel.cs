﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FreshMvvm;
using System.Collections.ObjectModel;
using App1.Model;
using Xamarin.Forms;
using App1.Interface;
using App1.Pages;

namespace App1.PageModels
{
    class MenuItemPageModel: FreshBasePageModel
    {
        private string _fio { get; set; }
        public string FIO
        {
            get
            {
                return _fio;
            }
            set
            {
                _fio = value;
            }
        }
        private string _image { get; set; }
        public string Image
        {
            get
            {
                return _image;
            }
            set
            {
                _image = value;
            }
        }
        private ObservableCollection<MenuItemModel> _menuList;

        public ObservableCollection<MenuItemModel> menuList
        {
            get { return _menuList; }
            set
            {
                _menuList = value;
            }
        }

        private MenuItemModel _selectedItem { get; set; }
        public MenuItemModel selectedItem
        {
            get => _selectedItem;
            set
            {
                _selectedItem = value;

                _selectedItem?.Command.Execute(true);
            }
        }

        public MenuItemPageModel()
        {
            menuList = new ObservableCollection<MenuItemModel>()
            {
                new MenuItemModel("Logout","icon.png", () => NavigateTo("Logout")),
                new MenuItemModel("EditCompany","icon.png", () => NavigateTo("GetFriend"))
            };
        }

        private LoginResult LoginResult { get; set; }

        public override void Init(object initData)
        {
            LoginResult = initData as LoginResult;

            FIO = string.Concat(LoginResult.FirstName," ", LoginResult.LastName);
            Image = LoginResult.ImageUrl;
        }
        private async Task NavigateTo(string com)
        {
            switch (com)
            {
                case "Logout":
                    switch (LoginResult.TypeSocial)
                    {
                        case TypeSocial.Facebook:
                            DependencyService.Get<IFacebookServiceAut>().Logout();
                            break;
                        case TypeSocial.Vk:
                            DependencyService.Get<IVkServiceAut>().Logout();
                            break;
                        case TypeSocial.Google:
                            DependencyService.Get<IGoogleService>().Logout();
                            break;
                    }
                    await App.Current.MainPage.Navigation.PopModalAsync();
                    break;
            }
            
            (App.Current.MainPage.Navigation.ModalStack[0] as FreshMasterDetailNavigationContainer).IsPresented = false;
        }

        public Command InfoUserCommand
        {
            get
            {
                return new Command(item =>
                {
                    Tuple<string, TypeSocial> tuple = null;

                    switch (LoginResult.TypeSocial)
                    {
                        case TypeSocial.Vk:
                            tuple = new Tuple<string, TypeSocial>(LoginResult.Token, TypeSocial.Vk);
                            break;
                        case TypeSocial.Facebook:
                            tuple = new Tuple<string, TypeSocial>(LoginResult.Token, TypeSocial.Facebook);
                            break;
                        case TypeSocial.Google:
                            tuple = new Tuple<string, TypeSocial>(LoginResult.Token, TypeSocial.Google);
                            break;
                    }

                    CoreMethods.PushPageModel<InfoUserPageModel>(tuple);

                    (App.Current.MainPage.Navigation.ModalStack[0] as FreshMasterDetailNavigationContainer).IsPresented = false;
                });
            }
        }
    }
}
