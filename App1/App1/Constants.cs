﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1
{
   public class Constants
    {

        //Google
        public static string AppName = "com.companyname.App1";

        public static string AndroidClientId = "6673259735-vcev6a4lki7ae5s0rglv34n730unec1p.apps.googleusercontent.com";

        public static string ClientSecret = null;

        public static string Scope = "https://www.googleapis.com/auth/userinfo.email";
        public static string AuthorizeUrl = "https://accounts.google.com/o/oauth2/auth";
        public static string AccessTokenUrl = "https://accounts.google.com/o/oauth2/token";
        public static string UserInfoUrl = "https://www.googleapis.com/oauth2/v2/userinfo";


        public static string AndroidRedirectUrl = "com.googleusercontent.apps.6673259735-vcev6a4lki7ae5s0rglv34n730unec1p:/oauth2redirect";


        //Microfost
        //public static string AppName = "com.companyname.App1";

        //public static string AndroidClientId = "d636bb33-3f2a-44d2-909e-756e448aa363";
        //public static string ClientSecret = null;


        //public static string Scope = "wl.basic, wl.emails, wl.photos";
        //public static string AuthorizeUrl = "https://login.live.com/oauth20_authorize.srf";
        //public static string AccessTokenUrl = "https://login.live.com/oauth20_token.srf";
        //public static string UserInfoUrl = "https://www.googleapis.com/oauth2/v3/userinfo";


        //public static string AndroidRedirectUrl = "https://login.live.com/oauth20_desktop.srf";


        //Facebook
        //public static string AppName = "com.companyname.App1";

        //public static string AndroidClientId = "287588715088758";

        //public static string ClientSecret = null;


        //public static string Scope = "public_profile,email,user_about_me";
        //public static string AuthorizeUrl = "https://m.facebook.com/dialog/oauth/";

        ////public static string AuthorizeUrl = "https://graph.facebook.com/oauth/authorize";

        //public static string AccessTokenUrl = "https://m.facebook.com/dialog/oauth/token";
        //public static string UserInfoUrl = "";


        //public static string AndroidRedirectUrl = "https://www.facebook.com/connect/login_success.html";

    }
}
