﻿using App1.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1.Interface
{
    public interface IVkServiceAut
    {
        Task<LoginResult> Login();
        void Logout();
    }
}
