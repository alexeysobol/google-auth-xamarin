﻿using App1.Facebook;
using App1.Facebook.Intefrace;
using App1.Facebook.Interface;
using App1.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

[assembly: Dependency(typeof(FacebookService))]
namespace App1.Facebook
{
    public class FacebookService : IFacebookService
    {
        private readonly IFacebookClient _facebookClient;

        public FacebookService()
        {
            var facebookClient = new FacebookClient();
            _facebookClient = facebookClient;
        }

        public async Task<InfoUserModel> GetAccountAsync(string accessToken)
        {
            var result = await _facebookClient.GetAsync<dynamic>(
                accessToken, "me", "fields=id,name,email,first_name,last_name,age_range,birthday,gender,locale, picture.width(1000).height(1000)");

            if (result == null)
            {
                return new InfoUserModel();
            }

            var account = new InfoUserModel
            {
                Id = result.id,
                Email = result.email ?? "-",
                Name = result.name ?? "-",
                UserName = result.username ?? "-",
                FirstName = result.first_name ?? "-",
                LastName = result.last_name ?? "-",
                Locale = result.locale ?? "-",
                Photos = result?.picture.data.url ?? "-",
                Gender = result?.gender ?? "-",
                Birthday = result?.birthday ?? "-"

            };

            return account;
        }

        public async Task PostOnWallAsync(string accessToken, string message)
            => await _facebookClient.PostAsync(accessToken, "me/feed", new { message });
    }
}
