﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1.Model
{
    public class StringValueAttribute : Attribute
    {
        private string _value;

        public StringValueAttribute(string value)
        {
            _value = value;
        }

        public string Value
        {
            get { return _value; }
        }
    }
    public enum EGender
    {
        male,
        famale
    }
    public class InfoUserModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public string Locale { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Birthday { get; set; }
        public string Photos { get; set; }
        private string _gender { get; set; }
        public string Gender
        {
            get
            {
                return _gender;
            }
            set
            {
                _gender = value != "-" ? (value == EGender.male.ToString()) || (value == "2") ? "Мужчина" : "Женщина" : value;
            }
        }
    }
}
