﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace App1.Model
{
    public class MenuItemModel
    {
        public string Title { get; set; }
        public string Icon { get; set; }
        public Command Command { get; set; }

        public MenuItemModel(string title, string icon, Func<Task> commandAction)
        {
            Title = title;
            Icon = icon;
            Command = new Command(() => commandAction());
        }
    }
}
