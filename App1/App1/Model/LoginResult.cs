﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1.Model
{

    [JsonObject]
    public class LoginResult
    {

        [JsonProperty("given_name")]
        public string FirstName { get; set; }
        [JsonProperty("family_name")]
        public string LastName { get; set; }

        [JsonProperty("picture")]
        public string ImageUrl { get; set; }

        [JsonProperty("id")]
        public string UserId { get; set; }
        public string Token { get; set; }
        public DateTimeOffset ExpireAt { get; set; }
        public LoginState LoginState { get; set; }
        public TypeSocial TypeSocial { get; set; }
        public string ErrorString { get; set; }
    }
    public enum LoginState
    {
        Failed,
        Canceled,
        Success
    }
    public enum TypeSocial
    {
        Vk,
        Facebook,
        Google
    }


}
