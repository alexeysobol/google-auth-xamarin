﻿using App1.Google;
using App1.Google.Interface;
using App1.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

[assembly: Dependency(typeof(GoogleServices))]
namespace App1.Google
{
    public class GoogleServices : IGoogleService
    {
        private readonly IGoogleClient _facebookClient;

        public GoogleServices()
        {
            var facebookClient = new GoogleClient();
            _facebookClient = facebookClient;
        }

        public async Task<InfoUserModel> GetAccountAsync(string accessToken)
        {
            var result = await _facebookClient.GetAsync<dynamic>(
                accessToken, "userinfo", "");

            if (result == null)
            {
                return new InfoUserModel();
            }

            var account = new InfoUserModel
            {
                Id = result.id,
                Email = result.email ?? "-",
                Name = result.name ?? "-",
                UserName = result.username ?? "-",
                FirstName = result.given_name ?? "-",
                LastName = result.family_name ?? "-",
                Locale = result.locale ?? "-",
                Photos = result?.picture ?? "-",
                Gender = result?.gender ?? "-",
                Birthday = result?.birthday ?? "-"

            };

            return account;
        }
    }
}
