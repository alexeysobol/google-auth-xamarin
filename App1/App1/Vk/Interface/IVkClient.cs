﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1.Vk.Interface
{
    interface IVkClient
    {
        Task<T> GetAsync<T>(string accessToken,string endpoint, string args = null);
    }
}
