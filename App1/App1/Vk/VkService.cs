﻿using App1.Model;
using App1.Vk;
using App1.Vk.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

[assembly: Dependency(typeof(VkService))]
namespace App1.Vk
{
    public class VkService: IVkService
    {
        private readonly IVkClient _vkClient;

        public VkService()
        {
            var vkClient = new VkClient();
            _vkClient = vkClient;
        }

        public async Task<InfoUserModel> GetAccountAsync(string accessToken)
        {
            var result = await _vkClient.GetAsync<dynamic>(
                 accessToken, "users.get", "bdate,photo_200,sex,city");

            if (result == null)
            {
                return new InfoUserModel();
            }
            result = result["response"][0];

            var account = new InfoUserModel
            {
                Id = result.id,
                Email = result.email ?? "-",
                UserName = result.last_name ?? "-",
                LastName = result.last_name ?? "-",
                FirstName = result.first_name ?? "-",
                Name = string.Concat(result.first_name, " ", result.last_name) ?? "-",
                Locale = result.city.title ?? "-",
                Photos = result?.photo_200 ?? "-",
                Gender = result?.sex ?? "-",
                Birthday = result?.bdate ?? "-"

            };

            return account;
        }
    }
}
